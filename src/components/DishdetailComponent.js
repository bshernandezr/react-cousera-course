import moment from 'moment';
import React, { Component } from 'react';
import { Control, Errors, LocalForm } from 'react-redux-form';
import { Link } from 'react-router-dom';
import { Button, Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, CardImg, CardText, Col, Label, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';

function RenderDish({dish}) {
    return (
        <Card>
            <CardImg top src={baseUrl + dish.image} alt={dish.name} />
            <CardBody>
                <CardHeader>{dish.name}</CardHeader>
                <CardText>{dish.description}</CardText>
            </CardBody>
        </Card>
    );
}

function RenderComments({comments, addComment, dishId}) {
    const commentsList = comments.map(comment => {
        return (
            <li key={comment.id}>
                <p>{comment.comment}</p>
                <p>-- {comment.author}, {moment(comment.date).format('MMMM Do YYYY')} </p>
            </li>
        );
    });
    return (
        <React.Fragment>
            {commentsList}
            <li>
                <CommentForm dishId={dishId} addComment={addComment}></CommentForm>
            </li>
        </React.Fragment>
    );
}

const Dishdetail = (props) => {
        console.log(props);
        if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">            
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">            
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if (props.dish != null)  {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem> <Link to="/menu">Menu</Link> </BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>
                </div>
                <div className="row">                
                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish} />
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <h1>Comments</h1>
                        <ul className="list-unstyled">
                            <RenderComments 
                            comments={props.comments}
                            addComment={props.addComment}
                            dishId={props.dish.id}/>
                        </ul>
                    </div>              
                </div>
            </div>
            
        );
        } else {
            return (<div></div>);
        }

}

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);
class CommentForm extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          isModalOpen: false
        };
        this.toggleModal = this.toggleModal.bind(this);
      }    

    
      toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      }
      
      handleSubmit(values) {
        this.toggleModal();
        this.props.addComment(this.props.dishId, values.rating, values.author, values.comment);
        alert(JSON.stringify(values));
      }
      
      render() {
        return (
          <React.Fragment>   
            <Button onClick={this.toggleModal} color="light"> <span className="fa fa-pencil"></span> Submit Comment </Button>         
            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
              <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
              <ModalBody className="align-items-center">
                  <LocalForm onSubmit={(values) => this.handleSubmit(values) }>
                    <Row className="form-group">
                        <Col>
                            <Label htmlFor="rating">Rating</Label>
                            <Control.select model=".rating" name="rating" className="form-control"
                            validators={{required}}>
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </Control.select>
                            <Errors className="text-danger"
                            model=".rating"
                            show="touched"
                            messages={{
                                required: 'Required'
                            }}
                            />
                        </Col>                        
                    </Row>
                    <Row className="form-group">
                        <Col>
                            <Label htmlFor="author">Author</Label>
                            <Control.text model=".author" name="author" className="form-control"
                             validators={{
                                required, minLength: minLength(3), maxLength: maxLength(15)
                            }}>
                            </Control.text>
                            <Errors
                            className="text-danger"
                            model=".author" 
                            show="touched"
                            messages={{
                                required: 'Required',
                                minLength: 'Must be greater than 2 characters',
                                maxLength: 'Must be 15 characters or less'
                            }}/>
                        </Col>                        
                    </Row>
                    <Row className="form-group">
                        <Col>
                            <Label htmlFor="comment">Comment</Label>
                            <Control.textarea model=".comment" name="comment" className="form-control"
                            validators={{required}}>
                            </Control.textarea>
                            <Errors className="text-danger"
                            model=".comment"
                            show="touched"
                            messages={{
                                required: 'Required'
                            }}
                            />
                        </Col>                        
                    </Row>
                    <Row className="form-group">
                        <Col>
                            <Button type="submit" value="submit" color="primary">Submit</Button>
                        </Col>                        
                    </Row>
                  </LocalForm>               
              </ModalBody>
            </Modal>
          </React.Fragment>
        );
      }
    }


export default Dishdetail;